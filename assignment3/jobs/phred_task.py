#!/usr/bin/env python3

"""
Job containing a Data class, map function and reduce function.

The data classes parses a file to create equally sized chunks to process.

The mapper function calculates the average phred score for that data chunk

The reducer calculates the average phred score from each map function and writes that to an output file
"""


# IMPORTS
import csv
import errno
import math
import os


# CLASSES
# Data class for task input
class Data:
    """Data class for creating Data objects which can be used to start a task"""
    def __init__(self, fastq_file, tasks):
        """
        Init collecting and assigning the input to self
        :parameter fastq_file: The file to process the chunk from
        :parameter tasks: total amount of tasks to be executed
        """
        self.fastq_file = fastq_file
        self.tasks = tasks

        # Creating the data chunks
        total_bytes = os.path.getsize(self.fastq_file)
        chunk_size = math.ceil(total_bytes / tasks)

        self.chunks = [i for i in range(0, total_bytes, chunk_size)]
        self.chunks.append(total_bytes)

    def __repr__(self):
        """Altering string representation"""
        return f"Data object for parsing the bytes in {self.fastq_file} spread over {self.tasks} tasks"

    def __iter__(self):
        """Iterator protocol"""
        self.i = 0
        return self

    def __next__(self):
        """Iterator protocol"""
        # Calculating the start and stop values
        if not self.i+1 == len(self.chunks):
            data = tuple([self.chunks[self.i], self.chunks[self.i+1]])
            self.i += 1
            return data
        else:
            raise StopIteration("Final chunk already returned, no data left!")

    def get_filename(self):
        """Filename getter"""
        return self.fastq_file


# FUNCTIONS
def get_avg_phreds(input_key, data):
    """
    Opens the file as bytes, and processes the designated chunk from that file.
    :returns: The average phred score per base position in that chunk (or add it to InterMediateValue bus?)
    """
    # The filename will serve as the key
    # Finding the first complete header and setting that as new start
    with open(input_key, "rb") as fastq_file:

        # If the start byte is not 0, find the nearest upcoming header
        fastq_file.seek(data[0])
        if not data[0] == 0:
            start_bytes = []
            for line in fastq_file:
                start_bytes.append(fastq_file.tell())
                if line.decode("utf-8").startswith("@DE18PCC"):
                    break
            fastq_file.seek(start_bytes[-2])

        # Loop over all the lines to find the quality strings
        phreds_dict = {}
        for count, line in enumerate(fastq_file, start=1):
            if line.decode("utf-8").startswith("@DE18PCC"):
                if fastq_file.tell() > data[1]:
                    break

            # When the quality string has been found, process it
            if count % 4 == 0:
                qualities = list(line)[:-1]
                for base_nr, quality in enumerate(qualities, start=1):
                    if base_nr not in phreds_dict:
                        phreds_dict[base_nr] = [quality]
                    else:
                        phreds_dict[base_nr].append(quality)

    # Returning the avg phred for that position as lists in a list
    return [[key, round(sum(phreds_dict[key]) / len(phreds_dict[key]))] for key in phreds_dict]


def map(input_key, data):
    """Get the average phred values of each base position for the chunks and save as intermediate result"""
    avg_phreds = get_avg_phreds(input_key, data)
    return input_key, avg_phreds


def reduce(output_key, results):
    """
    Get the average phred scores per base per chunk from the intermediate result queue,
    and calculate the average phred score per base over all the chunks.
    These are then written to the output file, based on the filename (output key)
    """
    avg_phreds = {}

    # Iterating over each result
    for result in results:
        result = result['result']

        result = result.get_intermediate_value()
        result = result[1]

        # Appending the average for that base to the correct list in the dict
        for phred in result:
            if phred[0] not in avg_phreds:
                avg_phreds[phred[0]] = [phred[1]]
            else:
                avg_phreds[phred[0]].append(phred[1])

    # Turning the average scores into a list
    avg_phreds = [[key, (sum(avg_phreds[key]) / len(avg_phreds[key]))-33] for key in avg_phreds]

    # Creating the output file / directory
    outfile = output_key.split("/")[-1].split(".")[0]
    outfile = f"jobs/output/{outfile}.csv"

    if not os.path.exists(os.path.dirname(outfile)):
        try:
            os.makedirs(os.path.dirname(outfile))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise

    # Writing to the output file
    with open(outfile, "w", newline="") as out:
        out_writer = csv.writer(out)
        out_writer.writerows(avg_phreds)

    # Return finished status
    return "finished"
