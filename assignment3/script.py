#!/usr/bin/env python3

"""
Script.py

Third assignment of Big Data Computing

Calculates the average PHRED score per base position for all reads within a fastq file and writes
these to an output file

In order to makes this process as fast as possible, these processes are spread over different computers,
and different cores using mapreduce.
"""


__author__ = "Jamie van Eijk -- 390103"
__version__ = 1.0


# IMPORTS
# Multiprocessing packages
import multiprocessing as mp
from multiprocessing.managers import BaseManager

# Other important packages
import argparse
import importlib
import os
import queue
import subprocess
import sys
import time


# PUBLIC VARIABLES
POISONPILL = "YOU DIED"
ERROR = "Mistakes were Made"


# CLASSES
# Master class overseeing the processes
class Master:
    def __init__(self, hosts, port, authkey, observer):
        """
        Init collecting and assigning the input to self
        :parameter hosts: list with hosts for server and clients
        :parameter port: The port where everything is running on / connected to
        :parameter authkey: The authentication key for this hosting
        :parameter observer: The name of the directory to watch
        """
        self.hosts = hosts
        self.port = port
        self.authkey = authkey
        self.observer = observer

        # Queues
        self.storage = MessageBus(hosts[0], port, authkey, status="s")

    def run(self, filename, num_processes):
        """
        Starting the server, which iterates over the job files found by the observer
        These files are then imported and tasks are build using the classes/functions within the jobs
        """

        # Calculating the total amount of tasks
        total_tasks = len(self.hosts[1:]) * num_processes
        print(f"Running {total_tasks} tasks over {len(self.hosts[1:])} clients using {num_processes} cores")

        # Iterating over each job
        python_files = self.observer.get_directory_files()
        for file in python_files:
            file = file.split(".")
            if file[-1] == "py":

                # Importing the job to get the Data object
                print(f"Importing file: {file[0]}")
                module = importlib.import_module(f"{file[0]}")
                data = module.Data(filename, total_tasks)

                # Creating tasks and placing them in the queue
                print("Filling task queue")
                data = iter(data)
                for chunk in data:
                    task = Task(file[0], "map", filename, chunk)
                    print(f"adding task: {task}")
                    self.storage.put_task(task)

                # Result container
                results = []
                while True:
                    # Trying to get the results from the results queue
                    try:
                        result = self.storage.get_result()
                        results.append(result)
                        print(f"Got result {result} from result queue!")

                        # If all chunks have been processed, add the reduce task
                        if len(results) == total_tasks:
                            print("All chunks have been processed and added to the results, mapping finished!")
                            self.storage.put_task(Task(file[0], "reduce", filename, results))
                            print("Starting reduce task!")
                            self.storage.put_task(POISONPILL)
                            results = []

                        # When the reduce task is finished, exit the loop
                        if result["result"] == "finished":
                            print("Reduce finished, shutting down...")
                            break

                    # If no results are found in the queue, wait a moment before trying again
                    except queue.Empty:
                        time.sleep(1)
                        continue

        # Killing the server
        self.storage.die()

    def get_port(self):
        """Returns the corrected port for clients to use"""
        return self.storage.get_correct_port()


# Client class running workers
class Client:
    """Client class which runs on an assigned computer making sure the workers are activated and managed"""
    def __init__(self, ip, port, authkey):
        """
        Init collecting and assigning the input to self
        :parameter ip: IP of the client
        :parameter port: port of the server to connect to
        :parameter authkey: Authentication key for the server
        """
        self.ip = ip
        self.port = port
        self.authkey = authkey

        # Queues
        self.storage = self.storage = MessageBus(ip, port, authkey, status="c")

    def __repr__(self):
        """Altering string representation"""
        return f"Client connected to {self.ip}:{self.port}"

    def run_workers(self, num_processes):
        """Workers are started equal to the requested amount of processes"""
        processes = []

        # For each process, create a worker
        for p in range(num_processes):
            worker = Worker(self.storage)
            temP = mp.Process(target=worker.start)
            processes.append(temP)
            temP.start()
        for temP in processes:
            temP.join()


# Worker class used to create workers which can execute the tasks
class Worker:
    """
    Worker with access to the task and result queue message busses.
    If there is a task available, the worker executes it and places the
    result in the result queue. If all tasks are finished, a poison pill is consumed,
    killing the worker
    """
    def __init__(self, storage):
        """
        Init collecting and assigning the input to self
        :parameter storage: Storage object containing a message bus with the task and result queues
        """
        self.storage = storage
        self.name = mp.current_process().name

    def __repr__(self):
        """Altering string representation"""
        return f"Worker called: {self.name}"

    def start(self):
        """Getting jobs from the task queue, until everything is done and the worked can die"""
        while True:

            # If the POISONPILL has arrived, all tasks are finished and the worker is exterminated
            try:
                task = self.storage.get_task()

                if task == POISONPILL:
                    self.storage.put_task(POISONPILL)
                    print(f"You Died: {self.name}")
                    return

                # Else execute the task and append to result queue
                else:
                    # If the name is valid, do your thing
                    try:
                        # If the task is a mapping task
                        if task.func == "map":
                            print(f"Worker {self.name} busy with {task}!")
                            key, result = task.do_task()
                            intermediate_value = IntermediateValue(key, result)
                            self.storage.put_result({'task': task, 'result': intermediate_value})
                            print(f"Worker {self.name} Finished!, Getting new task, or Dying!")

                        # If the task is a reduce task
                        elif task.func == "reduce":
                            print(f"Worker {self.name} busy with {task}!")
                            result = task.do_task()
                            self.storage.put_result({'task': task, 'result': result})
                            print(f"Worker {self.name} Finished!, Getting new task, or Dying!")

                    # If the name is not valid, append error
                    except NameError:
                        print("Worker cannot be found... where could he be?")
                        self.storage.put_result({'task': task, 'result': ERROR})

            # If there are no tasks, but also no death, just take some rest
            except queue.Empty:
                print(f"Take some rest {self.name}, it was a long day")
                time.sleep(1)


# Task class providing tasks for the worker to do
class Task:
    """Creates a task object which the worker can use to use to run a mapping or reducing task"""

    def __init__(self, module, func, key, data):
        """
        Init collecting and assigning the input to self
        :parameter module: The module that should be imported, containing the func
        :parameter func: The function that the task should execute
        :parameter key: The key for that task
        :parameter data: Data object containing the filename, start_byte and end_byte
        """
        self.module = module
        self.func = func
        self.key = key
        self.data = data

    def __repr__(self):
        """Altering string representation"""
        return f"Task object doing {self.func} from {self.module} using the key {self.key} on the data: {self.data}"

    def do_task(self):
        """Running the requested task and returning the output"""
        module = importlib.import_module(self.module)
        func = getattr(module, self.func)
        return func(self.key, self.data)


# Observer class monitoring the directory
class Observer:
    """Observer watching a directory, and giving the changes to master"""

    def __init__(self, directory):
        """
        Init collecting and assigning the input to self
        :parameter directory: The directory to watch
        """
        self.directory = directory

    def __repr__(self):
        """Altering string representation"""
        return f"Observing {self.directory}"

    def get_directory_files(self):
        """Checking the jobs directory for python file"""
        files = [file for file in os.listdir(self.directory)
                 if os.path.isfile(os.path.join(self.directory, file))]

        print(f"Jobs directory contains: {files}")
        return files

    def get_directory(self):
        """Getting the directory name"""
        return self.directory


# Message Bus class for the task, intermediate results and result queues
class MessageBus:
    """Message bus object which will serve as a public data storage for the processes"""

    def __init__(self, ip, port, authkey, status):
        """
        Init collecting and assigning the input to self
        :parameter ip: The ip of the server on which the message bus should be hosted
        :parameter port: The port of the server on which the message bus should be hosted
        :parameter authkey: The authkey of the server on which the message bus should be hosted
        :parameter status: Are you a server (s) or client (c)
        """
        self.ip = ip
        self.port = port
        self.authkey = authkey

        # Bus manager
        if status == "s":
            self.manager = self.__start_server_bus()
        else:
            self.manager = self.__connect_to_bus()

        # Queue object
        self.task_queue = self.manager.get_task_queue()
        self.result_queue = self.manager.get_result_queue()

    def __start_server_bus(self):
        """Creating a manager for the message bus that will contain the task and result queues"""
        # QueueManager object inheriting the BaseManager

        task_queue = queue.Queue()
        result_queue = queue.Queue()

        class QueueManager(BaseManager):
            pass

        QueueManager.register('get_task_queue', callable=lambda: task_queue)
        QueueManager.register('get_result_queue', callable=lambda: result_queue)

        # Starting server manager and returning it to self
        while True:
            try:
                print(f"Hosting on {self.ip}:{self.port} using {self.authkey}")
                manager = QueueManager(address=(self.ip, self.port), authkey=self.authkey)
                manager.start()
                print('Server starting using port %s' % self.port)
                break

            # If port already in use, try the next number
            except (OSError, EOFError) as e:
                print(f"Port: {self.port} is already in use, checking if the next one is free")
                self.port += 1

        return manager

    def __connect_to_bus(self):
        """Connects the IP of the client to the IP and port of the host"""
        # Create ServerQueueManager class using the BaseManager as mother class
        class ServerQueueManager(BaseManager):
            pass

        ServerQueueManager.register('get_task_queue')
        ServerQueueManager.register('get_result_queue')

        # Starting client manager and returning it
        print(f"connecting to {self.ip}:{self.port} using {self.authkey}")
        manager = ServerQueueManager(address=(self.ip, self.port), authkey=self.authkey)
        manager.connect()

        print(f"Client has been connected to {self.ip}:{self.port}")

        return manager

    def get_task(self):
        """Gets the first item from the queue (FIFO)"""
        return self.task_queue.get_nowait()

    def get_result(self):
        """Gets the first item from the queue (FIFO)"""
        return self.result_queue.get_nowait()

    def put_task(self, item):
        """Puts a new item into the queue (FIFO)"""
        self.task_queue.put(item)

    def put_result(self, item):
        """Puts a new item into the queue (FIFO)"""
        self.result_queue.put(item)

    def get_correct_port(self):
        """Returns the corrected port for clients to use"""
        return self.port

    def die(self):
        """Kills the server / client"""
        self.manager.shutdown()


# Intermediate Value class for transfer between mapper and reducer
class IntermediateValue:
    """
    After the map function calculated the average phred score per base for a chunk, they can be saved here
    to be passes to a reducer function which calculates the average phred score over all the chunks,
    and thus the whole file
    """

    def __init__(self, key, intermediate_value):
        """
        Init collecting and assigning the input to self
        :parameter key: The key for that task
        :parameter intermediate_value: Intermediate value to save alongside key
        """
        self.key = key
        self.intermediate_value = intermediate_value

    def __repr__(self):
        """Altering string representation"""
        return f"Intermediate value with key: {self.key} and value: {self.intermediate_value}"

    def get_intermediate_value(self):
        """Returns the Intermediate value as a list with the key and data"""
        return [self.key, self.intermediate_value]


# MAIN
def main(argv=None):
    """Main function parsing the commandline and executing the functions"""

    # Commandline set-up
    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument('-f', '--fastq', type=str, required=True,
                        help="The fastQ file to be processed")

    parser.add_argument('-n', '--num_cores', type=int, required=True,
                        help="Integer for the number of cores that should be used per client")

    parser.add_argument('-h', '--hosts', nargs="*", action="append", required=True,
                        help="The hosts for the process, with the first as the server, and the second as the client")

    parser.add_argument('-p', '--port', type=int, required=True,
                        help="The port of the host")

    server_or_client = parser.add_mutually_exclusive_group(required=True)

    server_or_client.add_argument('-c', '--client', action="store_true",
                                  help="Use -c or --client as argument if this is a client")

    server_or_client.add_argument('-s', '--server', action="store_true",
                                  help="Use -s or --server as argument if this is a server")

    parser.add_argument('-d', '--jobsdirectory', type=str, required=True,
                        help="The directory to be monitored, in which the jobs and output file will be placed")

    parser.add_argument('-r', '--retries', type=str, required=True,
                        help="If there are problems, how many times should the program try")

    # Getting the arguments and placing them in the correct variables
    args = parser.parse_args()

    filename = args.fastq
    num_cores = args.num_cores
    hosts = args.hosts[0]
    port = args.port
    job_dir = args.jobsdirectory
    retries = args.retries
    server = args.server
    client = args.client

    # Setting the outkey and working directory
    authkey = b'justyourregularauthkey'
    sys.path.append(job_dir)

    # Starting the server, if this is the main computer
    if server:
        observer = Observer(job_dir)
        master = Master(hosts, port, authkey, observer)
        port = master.get_port()
        time.sleep(2)

        # Starting client computers to do tasks
        for computer in hosts[1:]:
            hosts = [hosts[-1]] + hosts[:-1]
            command = ["ssh", f"{computer}", "python3", "-u", "-", "-f", f"{filename}", "-n", f"{num_cores}",
                       "-h", f"{' '.join(hosts)}", "-p", f"{port}", "-c", "-d", f"{job_dir}", "-r", f"{retries}",
                       "<", "Documents/big_data_computing/assignment3/script.py"]
            print(f"Starting subprocess: {command}")
            subprocess.run(command)

        # Filling the queues with data and awaiting results
        master.run(filename, num_cores)
        time.sleep(2)

    # If this is a client computer, start a client
    if client:
        client = Client(hosts[0], port, authkey)  # Hosts list is manipulated in a way that the client ip is first
        client.run_workers(num_cores)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
