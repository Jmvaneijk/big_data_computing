#!/usr/bin/env python3

"""
Script.py

Second assignment of Big Data Computing

Calculates the average PHRED score per base position for all reads within a fastq file.
These can also be written to an output file, if specified by the user.

In order to makes this process as fast as possible, these processes are spread over different computers,
and different cores using Client and Server structures.

python3 script.py -f "../data/rnaseq.fastq" -n 4 -h "" "" -p 25665 -c -o phreds.csv
"""

__author__ = "Jamie van Eijk -- 390103"
__version__ = 2.0


# Multiprocessing packages
import multiprocessing as mp
from multiprocessing.managers import BaseManager

# Other important packages
import argparse
import csv
import os
import queue
import sys
import time
import math

# PUBLIC VARIABLES
POISONPILL = "YOU DIED"
ERROR = "Mistakes were Made"
IP = []
PORTNUM = 0
AUTHKEY = b'justyourregularauthkey'
OUTFILE = ''


# SERVER SIDED
# Server Runner
def server_runner(function, chunks, filename):
    """
    Starting a shared manager, which adds functions and data to the job queue.
    And fetches the results processed by the clients.
    """
    # Starting the shared manager server and accessing its queues
    manager = create_server_manager(PORTNUM, b'justyourregularauthkey')
    shared_job_queue = manager.get_job_queue()
    shared_result_queue = manager.get_result_queue()

    # Checking if there is data
    if not filename or not chunks:
        print("*Yawns* Im Bored.. Give me something")
        return

    # If the data is there, jobs are creating with a filename and their chunk of that file
    print("Sending data!")
    for i in range(len(chunks)):
        if not i+1 == len(chunks):
            shared_job_queue.put({'fn': function, 'data': (filename, chunks[i], chunks[i+1])})

    # After putting all the data in the Queue, the POISONPILL is added as final item
    shared_job_queue.put(POISONPILL)

    # Sleeping for 2 seconds to let the magic happen
    time.sleep(2)

    # Collecting the results
    results = []
    while True:

        # Trying to get the results from the results queue
        try:
            result = shared_result_queue.get_nowait()
            results.append(result)
            print(f"Got result {result} from result queue!")

            # If all chunks have been processed, exit the loop
            if len(results) == len(chunks)-1:
                print("All chunks have been processed and added to the results")
                break

        # If no results are found in the queue, wait a moment before trying again
        except queue.Empty:
            time.sleep(1)
            continue

    # Sleep for a moment to give the clients time to die
    print("Server is finished, Good work!!")
    manager.shutdown()

    # Print the results
    # print(results)

    # Write the results to output
    avg_phreds = process_mp_outputs(results)
    write_to_csv(OUTFILE, avg_phreds)


# Server Side Manager
def create_server_manager(port, authkey):
    """
    Creates a server sided manager, using the given port.
    :returns A manager object with get job and result queue methods
    """
    # Queues that will contain the jobs and results respectively
    job_queue = queue.Queue()
    result_queue = queue.Queue()

    # Creating synchronized proxies for the Queue objects
    class QueueManager(BaseManager):
        pass

    QueueManager.register('get_job_queue', callable=lambda: job_queue)
    QueueManager.register('get_result_queue', callable=lambda: result_queue)

    # Starting server manager and returning it
    while True:
        try:
            manager = QueueManager(address=(IP[0], port), authkey=authkey)
            manager.start()
            print('Server starting using port %s' % port)
            break
        except (OSError, EOFError):
            port += 1

    return manager


# CLIENT SIDED
# Client Side Manager
def create_client_manager(ip, port, authkey):
    """
    Creates a client sided manager, this manager connects to the server on that
    given address and exposes the get_job_queue and get_result_queue methods for
    accessing the shared queues from the server.
    :returns A manager object with get job and result queue methods
    """

    # Create ServerQueueManager class using the BaseManager as mother class
    class ServerQueueManager(BaseManager):
        pass

    # Registering the get job/result queue methods
    ServerQueueManager.register("get_job_queue")
    ServerQueueManager.register("get_result_queue")

    # Starting client manager and returning it
    manager = ServerQueueManager(address=(ip, port), authkey=authkey)
    manager.connect()

    print(f"Client has been connected to {ip}:{port}")
    return manager


# Client Runner
def client_runner(num_processes):
    """Starting a local client manager, using the IP, PORT and AUTHKEY to start workers"""
    manager = create_client_manager(IP[1], PORTNUM, AUTHKEY)
    job_q = manager.get_job_queue()
    result_q = manager.get_result_queue()
    run_workers(job_q, result_q, num_processes)


# Client Worker
def run_workers(job_queue, result_queue, num_processes):
    """On this local client, the requested amount of cores are turned into workers"""
    processes = []

    # For each process, create a worker
    for p in range(num_processes):
        temP = mp.Process(target=worker, args=(job_queue, result_queue))
        processes.append(temP)
        temP.start()
    print(f"Give something to do to {num_processes} workers")
    for temP in processes:
        temP.join()


# Single Worker
def worker(job_queue, result_queue):
    """Worker function executing a job and appending it to the result queue"""
    # Stores name of job
    my_name = mp.current_process().name

    # Job loop
    while True:

        # If the poisonpill has arrived, all tasks are finished and the worker is exterminated
        try:
            job = job_queue.get_nowait()

            if job == POISONPILL:
                job_queue.put(POISONPILL)
                print(f"You Died: {my_name}")
                return

            # Else execute the job and append to result queue
            else:
                # If the name is valid, do your thing
                try:
                    result = job['fn'](*job['data'])
                    print(f"Worker {my_name} busy with {job['data']}!")
                    result_queue.put({'job': job, 'result': result})
                    print(f"Worker {my_name} Finished!, Getting new job, or Dying!")

                # If the name is not valid, append error
                except NameError:
                    print("Worker cannot be found... where could he be?")
                    result_queue.put({'job': job, 'result': ERROR})

        # If there are no jobs, but also no death, just take some rest
        except queue.Empty:
            print(f"Take some rest {my_name}, it was a long day")
            time.sleep(1)


# FASTQ PROCESSING FUNCTIONS
def get_avg_phreds(fastq, start_byte, end_byte):
    """Opens the file, and processes its designated chunk"""
    # Finding the first complete header and setting that as new start
    with open(fastq, "rb") as fastq_file:

        # If the start byte is not 0, find the nearest upcoming header
        fastq_file.seek(start_byte)
        if not start_byte == 0:
            start_bytes = []
            for line in fastq_file:
                start_bytes.append(fastq_file.tell())
                if line.decode("utf-8").startswith("@DE18PCC"):
                    break
            fastq_file.seek(start_bytes[-2])

        # Loop over all the lines to find the quality strings
        phreds_dict = {}
        for count, line in enumerate(fastq_file, start=1):
            if line.decode("utf-8").startswith("@DE18PCC"):
                if fastq_file.tell() > end_byte:
                    break

            # When the quality string has been found, process it
            if count % 4 == 0:
                qualities = list(line)[:-1]
                for base_nr, quality in enumerate(qualities, start=1):
                    if base_nr not in phreds_dict:
                        phreds_dict[base_nr] = [quality]
                    else:
                        phreds_dict[base_nr].append(quality)

    # Returning the avg phred for that position as lists in a list
    return [[key, round(sum(phreds_dict[key]) / len(phreds_dict[key]))] for key in phreds_dict]


def process_mp_outputs(results):
    """Parsing the different output lists, and collecting the different averages in a dict"""
    avg_phreds = {}

    # Iterating over each result
    for result in results:
        result = result['result']

        # Appending the average for that base to the correct list in the dict
        for phred in result:
            if phred[0] not in avg_phreds:
                avg_phreds[phred[0]] = [phred[1]]
            else:
                avg_phreds[phred[0]].append(phred[1])

    # Returning the average phred score per base position for ALL qualities this time
    return [[key, (sum(avg_phreds[key]) / len(avg_phreds[key]))-33] for key in avg_phreds]


def write_to_csv(outfile, avg_phreds):
    """Writes the base number and average phred score for tht position to the csv"""
    with open(outfile, "w", newline="") as out:
        out_writer = csv.writer(out)
        out_writer.writerows(avg_phreds)


def main(argv=None):
    """Main function parsing the commandline and executing the functions"""

    # Commandline set-up
    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument('-f', '--fastq', type=str, required=True,
                        help="The fastQ file to be processed")

    parser.add_argument('-n', '--num_cores', type=int, required=True,
                        help="Integer for the number of cores that should be used per client")

    parser.add_argument('-h', '--hosts', nargs="*", action="append", required=True,
                        help="The hosts for the process, with the first as the server, and the second as the client")

    parser.add_argument('-p', '--port', type=int, required=True,
                        help="The port of the host")

    server_or_client = parser.add_mutually_exclusive_group(required=True)

    server_or_client.add_argument('-c', '--client', action="store_true",
                                  help="Use -c or --client as argument if this is a client")

    server_or_client.add_argument('-s', '--server', action="store_true",
                                  help="Use -s or --server as argument if this is a server")

    parser.add_argument('-o', '--outfile', type=str, required=True,
                        help="An .csv output file with the average PHRED value at that base position")

    # Getting the arguments and placing them in the correct variables
    args = parser.parse_args()

    filename = args.fastq
    num_cores = args.num_cores
    hosts = args.hosts[0]
    port = args.port
    outfile = args.outfile
    server = args.server
    client = args.client

    # Setting the port, hosts and outfile to global
    global PORTNUM
    global IP
    global OUTFILE

    PORTNUM = port
    IP = hosts
    OUTFILE = outfile

    # Getting the amount of chunks
    chunk_count = num_cores * (len(hosts) - 1)
    print(f"dividing data into {chunk_count} chunks")

    # Calculating the start and stop values
    total_bytes = os.path.getsize(filename)
    chunk_size = math.ceil(total_bytes / chunk_count)

    chunks = [i for i in range(0, total_bytes, chunk_size)]
    chunks.append(total_bytes)

    # Starting the server, if this is the main computer
    if server:
        server = mp.Process(target=server_runner, args=(get_avg_phreds, chunks, filename))
        server.start()
        time.sleep(1)

    # Starting the client if this is a worker computer
    if client:
        client = mp.Process(target=client_runner, args=(num_cores,))
        client.start()
        client.join()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
