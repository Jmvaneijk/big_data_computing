#!/usr/bin/env python3

"""
Script.py

First assignment of Big Data Computing

Calculates the average PHRED score per base position for all reads within a fastq file.
These can also be written to an output file, if specified by the user.
"""

__author__ = "Jamie van Eijk -- 390103"
__version__ = 1.0

import argparse
import csv
from multiprocessing import Pool, Manager
import os
import sys


class Fastq:
    """
    Fastq class allowing validation of input, parsing of fastq files, calculating phred scores and writing to csv
    """

    def __init__(self, fastq_file, num_processes, outfile=None):
        """
        Init setting the self parameters and validating input
        """
        self.fastq_file = fastq_file
        self.num_processes = num_processes
        self.outfile = outfile

        # Using a manager to allow shared storage
        self.phreds_dict = Manager().dict()

    def fetch_qualities(self):
        """Storing all the quality lines as lists in a big list, for easy chunk allocation"""
        # Opening the fastq file
        with open(self.fastq_file, "r") as fastq:
            # Turning each line into a list
            fastq = fastq.read().split("\n")

        # Returning the output as lists in a list
        return [fastq[i] for i in range(3, len(fastq), 4)]

    def collect_phreds(self, qualities):
        """Iterates over the quality scores and append them to the dict"""
        phreds_dict = {}
        for base_nr, quality in enumerate(qualities, start=1):

            # Phred score can be calculated by ASCII value - 33
            phred = ord(quality) - 33

            # Each phred is appended to a list belonging to the base number
            if base_nr not in phreds_dict:
                phreds_dict[base_nr] = [phred]
            else:
                phreds_dict[base_nr].append(phred)

        # Updates the manager dict using a local dict to improve speed, due to the slowness of managed structures
        self.phreds_dict.update(phreds_dict)

    def get_average_phreds(self):
        """Iterates over the collected phred scores and calculates the average for each position"""
        avg_phreds = []
        for key in self.phreds_dict.keys():
            avg_phred = sum(self.phreds_dict[key]) / len(self.phreds_dict[key])
            avg_phreds.append([key, avg_phred])
        return avg_phreds

    def write_to_csv(self):
        """Writes the base number and average phred score for tht position to the csv"""
        with open(self.outfile, "w", newline="") as out:
            out_writer = csv.writer(out)
            avg_phreds = self.get_average_phreds()
            out_writer.writerows(avg_phreds)


def validate_input(filename, num_processes, outfile):
    """Validates the input to prevent errors later, and notify the user of potential issues"""
    if not os.path.exists(filename):
        sys.exit(f"The file: {filename} does not exist or it could not be found, please try again...")

    if not num_processes > 0:
        sys.exit(f"The number of process should be bigger than 0, not {num_processes}, please try again...")

    if outfile is not None:
        if outfile.split(".")[-1] != "csv":
            sys.exit(f"The file should be in the CSV format, not {outfile}, please try again...")

    print("Input is valid, Starting Process..")


def main(argv=None):
    """Main function parsing the commandline and executing the functions"""

    # Commandline set-up
    parser = argparse.ArgumentParser()

    parser.add_argument("filename", type=str,
                        help="The name of the file")

    parser.add_argument('-n', '--num_processes', type=int, required=True,
                        help="Integer for the number of processes that should run")

    parser.add_argument('-o', '--outfile', type=str, nargs="?",
                        help="An .csv output file with the average PHRED value at that base position")

    # Getting the arguments and placing them in the correct variables
    args = parser.parse_args()

    filename = args.filename
    num_processes = args.num_processes
    outfile = args.outfile

    # Validating the provided input
    validate_input(filename, num_processes, outfile)

    # Creates fastq object
    fastq = Fastq(filename, num_processes, outfile)

    # Gets the list with the quality lines
    qualities = fastq.fetch_qualities()

    # Calculates the phreds spread over multiple processes
    with Pool(num_processes) as p:
        p.map(fastq.collect_phreds, qualities)

    # Writes the average scores to the outfile
    if outfile is not None:
        fastq.write_to_csv()

    # Else the scores are printed
    else:
        avg_phreds = fastq.get_average_phreds()
        print(avg_phreds)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
